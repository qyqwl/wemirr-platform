package com.wemirr.platform.bury.service;


import com.wemirr.framework.db.mybatisplus.ext.SuperService;
import com.wemirr.platform.bury.domain.entity.OptLog;

/**
 * @author Levin
 */
public interface OptLogService extends SuperService<OptLog> {
}
